﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Midterm
{
    class Program
    {
        static void Main(string[] args)
        {
            Exposition();
            Instructions();
            StartGame();
        }

        static void Exposition()
        {
            Console.WriteLine("One Day You Were In The Market Getting Food For Your Family When You");
            Console.WriteLine("Hear About A Legendary Warrior Who Died Navigating The Minatour's Maze");
            Console.WriteLine("And About The Treasure Still Left Inside. Only The Bravest Of People");
            Console.WriteLine("Attempt To Claim It. 'I Can Get That Treasure AND Kill The Minatour!'");
            Console.WriteLine("You Exclaim. You Return Home And Pack For The Long and Treacherous Journey");
            Console.WriteLine("Ahead. Your Plan Is To Use Your Wits To Outsmart The Minatour Because");
            Console.WriteLine("You're Too Poor To Own A Sword And Shield. You Travel Across The");
            Console.WriteLine("Desert To A Far Away Ruin Deep Underground");
            Console.WriteLine();
            Console.WriteLine("You Stand At The Entrance To The Labarynth.");
            Console.WriteLine("The Previous Travelers Died And Their Belongings Are Still Inside.");
            Console.WriteLine("Maybe Your Can Find Something To Defend Yourself With.");
            Console.WriteLine();
        }

        static void Instructions()
        {
            Console.WriteLine("INSTRUCTIONS");
            Console.WriteLine();
            Console.WriteLine("It Is Dark Inside But Your Memory Should Help You Navigate The Maze As You");
            Console.WriteLine("Learn All Its Twists And Turns. There Is A Cracked (S)hield Somewhere In The");
            Console.WriteLine("Maze. It Should Protect Your From A Single Hit By A (M)inatour So Use It Wisely.");
            Console.WriteLine("There Are 3 Minatours In Total. You're Going To Have To Defeat ALL Of Them To");
            Console.WriteLine("Escape The Maze Alive. One Guards A (K)ey, Another Guards The (D)oor Out, And");
            Console.WriteLine("The Other Guards The Only (W)eapon Strong Enough To Defeat Them. Navigate The");
            Console.WriteLine("Maze Carefully And You Just Might Make It Back Home To Your Family.");
            Console.WriteLine();
        }

        static void StartGame()
        {
            Console.WriteLine("Are You Ready To Begin? (Y/N)");
            ConsoleKey input = Console.ReadKey().Key;
            Console.WriteLine();
            switch (input)
            {
                case ConsoleKey.Y:
                    Console.Clear();
                    Maze maze = new Maze();
                    bool keepPlaying = true;
                    while (keepPlaying)
                    {
                        maze.DisplayMaze();
                        keepPlaying = maze.NextMove();
                    }
                    Console.ReadLine();
                    break;
                case ConsoleKey.N:
                    break;
                default:
                    Console.WriteLine("Invalid Input");
                    Exposition();
                    StartGame();
                    break;
            }
        }
    }
}
