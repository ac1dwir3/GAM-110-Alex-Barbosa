﻿using System;

namespace Midterm
{
    internal class Maze
    {
        //Player
        Player player;

        //Current Gameplay Info
        int enemiesDefeated;
        int goldCollected;

        //Maze Info
        int rowSize;
        int colSize;
        char[,] cells = {
            { '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' },
            { '#',' ',' ','#','#','#','#','#','#','#','#','#','#','#','#','#','$','#','#','#' },
            { '#',' ',' ','#','#',' ',' ',' ',' ',' ',' ','#','#','#','#','#',' ','#','#','#' },
            { '#','#',' ','#',' ',' ','#','#',' ','#',' ','#','#',' ',' ',' ',' ',' ',' ','#' },
            { '#','#',' ','#',' ','#',' ',' ',' ','#',' ',' ',' ',' ','#',' ','#','#',' ','#' },
            { '#','#',' ',' ',' ','#',' ','#','#','#',' ','#','#','#',' ','#','W','#',' ','#' },
            { '#','#','#',' ','#','#',' ','#','#','#',' ',' ',' ','#',' ','#','M',' ',' ','#' },
            { '#',' ',' ',' ','#','#',' ','#','#','#',' ','#',' ','#',' ','#',' ',' ',' ','#' },
            { '#',' ','#',' ',' ',' ',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ','#' },
            { '#',' ','#','#',' ','#','#','#','#','#',' ','#',' ','#',' ','#','#','#',' ','#' },
            { '#',' ','#','#',' ','$','#','#','#','#',' ','#','$','#',' ','#','#','#',' ','#' },
            { '#',' ','#','#','#','#',' ',' ',' ','#',' ','#','#',' ','M','D','#','#',' ','#' },
            { '#',' ',' ',' ','#','#',' ','#',' ','#',' ',' ','#',' ','#',' ',' ',' ','#','#' },
            { '#','#','#',' ','#','#',' ','#',' ',' ','#',' ','#',' ','#','#','#',' ','#','#' },
            { '#','#',' ',' ','#',' ','S',' ','#',' ',' ',' ',' ',' ',' ',' ','#',' ','#','#' },
            { '#','#',' ','#','#',' ','#',' ','$','#','#','#',' ','#','#',' ','#',' ','$','#' },
            { '#',' ',' ',' ',' ',' ',' ',' ','#','K','#','#',' ',' ',' ',' ','#',' ',' ','#' },
            { '#',' ','#',' ','#','#','#',' ','#',' ','#','#','#',' ','#',' ','#',' ','#','#' },
            { '#','$','#',' ',' ',' ',' ',' ','#','M',' ',' ',' ',' ','#','$','#',' ','E','#' },
            { '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' }
        };

        public Maze()
        {
            rowSize = cells.GetLength(0);
            colSize = cells.GetLength(1);

            player = new Player(1, 1);

            enemiesDefeated = 0;
        }

        internal void DisplayMaze()
        {
            Console.CursorVisible = false;

            //Displays Maze 5 Cells Around The Player
            //Creates A Mental Map Effect (What You've Seen Doesn't Disappear Filling In The Map As You Explore)
            for (int viewDistH = player.xPos - player.viewDistance; viewDistH < player.xPos + player.viewDistance && viewDistH < rowSize; viewDistH++)
            {
                if (viewDistH < 0)
                    viewDistH = 0;
                for (int viewDistV = player.yPos - player.viewDistance; viewDistV < player.yPos + player.viewDistance && viewDistV < colSize; viewDistV++)
                {
                    if (viewDistV < 0)
                        viewDistV = 0;
                    Console.SetCursorPosition(viewDistH, viewDistV);
                    switch (cells[viewDistH, viewDistV])
                    {
                        case '#':
                            Console.BackgroundColor = ConsoleColor.White;
                            Console.ForegroundColor = ConsoleColor.White;
                            break;
                        case 'D':
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.ForegroundColor = ConsoleColor.Black;
                            break;
                        case 'M':
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.Red;
                            break;
                        case 'K':
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.Green;
                            break;
                        case 'W':
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            break;
                        case 'S':
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            break;
                        case 'E':
                            Console.BackgroundColor = ConsoleColor.Cyan;
                            Console.ForegroundColor = ConsoleColor.Black;
                            break;
                        case '$':
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.DarkYellow;
                            break;

                        default:
                            Console.BackgroundColor = ConsoleColor.Black;
                            Console.ForegroundColor = ConsoleColor.White;
                            break;

                    }
                    Console.Write(cells[viewDistH, viewDistV]);
                }
            }

            //Displays Character
            Console.ResetColor();
            Console.SetCursorPosition(player.xPos, player.yPos);
            Console.WriteLine('P');

            //Displays Inventory
            Console.SetCursorPosition(rowSize + 1, 0);
            Console.Write("Has Shield: " + player.inventory[0] + ' ');
            Console.SetCursorPosition(rowSize + 1, 1);
            Console.Write("Has Weapon: " + player.inventory[1] + ' ');
            Console.SetCursorPosition(rowSize + 1, 2);
            Console.Write("Has Key: " + player.inventory[2] + ' ');
            Console.SetCursorPosition(rowSize + 1, 3);
            Console.Write("Gold Collected: " +goldCollected);
            Console.SetCursorPosition(rowSize + 1, 4);
            Console.Write("Enemies Defeated: " +enemiesDefeated);

        }

        internal bool NextMove()
        {
            bool keepPlaying = true;

            //Checks If Player Is Sharing A Cell With Anything
            switch (cells[player.xPos, player.yPos])
            {
                case 'S':
                    player.PickUpItem('S');
                    Console.ResetColor();
                    cells[player.xPos, player.yPos] = ' ';
                    break;
                case 'W':
                    player.PickUpItem('W');
                    Console.ResetColor();
                    cells[player.xPos, player.yPos] = ' ';
                    break;
                case 'M':
                    if (player.inventory[0] == true)
                    {
                        player.inventory[0] = false;
                        break;
                    }
                    if (player.inventory[1] == true)
                    {
                        Console.ResetColor();
                        cells[player.xPos, player.yPos] = ' ';
                        enemiesDefeated++;
                        goldCollected += 300;
                    }
                    if (player.inventory[0] == false && player.inventory[1] == false)
                    {
                        Console.ResetColor();
                        Console.SetCursorPosition(0, colSize + 1);
                        Console.WriteLine("You've Died...");
                        keepPlaying = false;
                    }
                    break;

                case '$':
                    Console.ResetColor();
                    cells[player.xPos, player.yPos] = ' ';
                    goldCollected += 100;
                    break;

                case 'K':
                    player.PickUpItem('K');
                    Console.ResetColor();
                    cells[player.xPos, player.yPos] = ' ';
                    break;
                case 'D':
                    Console.ResetColor();
                    cells[player.xPos, player.yPos] = ' ';
                    break;
                case 'E':
                    Console.ResetColor();
                    Console.SetCursorPosition(0, colSize + 1);
                    Console.WriteLine("You've Escaped The Maze!");
                    keepPlaying = false;
                    break;

                default:
                    break;
            }

            ConsoleKey input = Console.ReadKey().Key;
            switch (input)
            {
                //Quits Game
                case ConsoleKey.Q:
                    keepPlaying = false;
                    break;
                case ConsoleKey.Escape:
                    keepPlaying = false;
                    break;

                //Movement
                case ConsoleKey.UpArrow:
                    //Checks For Wall
                    if(!(cells[player.xPos,player.yPos - 1] == '#'))
                    {
                        //Checks For Door If Not Wall
                        if(cells[player.xPos, player.yPos - 1] == 'D')
                        {
                            //Checks Inventory For Key
                            if (player.inventory[2] == true)
                            {
                                player.MoveUp();
                                break;
                            }
                            else
                                break;
                        }
                        player.MoveUp();
                    }
                    break;
                case ConsoleKey.DownArrow:
                    if (!(cells[player.xPos, player.yPos + 1] == '#'))
                    {
                        if (cells[player.xPos, player.yPos + 1] == 'D')
                        {
                            if (player.inventory[2] == true)
                            {
                                player.MoveDown();
                                break;
                            }
                            else
                                break;
                        }
                        player.MoveDown();
                    }
                    break;
                case ConsoleKey.LeftArrow:
                    if (!(cells[player.xPos - 1, player.yPos] == '#'))
                    {
                        if (cells[player.xPos - 1, player.yPos] == 'D')
                        {
                            if (player.inventory[2] == true)
                            {
                                player.MoveLeft();
                                break;
                            }
                            else
                                break;
                        }
                        player.MoveLeft();
                    }
                    break;
                case ConsoleKey.RightArrow:
                    if (!(cells[player.xPos + 1, player.yPos] == '#'))
                    {
                        if (cells[player.xPos + 1, player.yPos] == 'D')
                        {
                            if (player.inventory[2] == true)
                            {
                                player.MoveRight();
                                break;
                            }
                            else
                                break;
                        }
                        player.MoveRight();
                    }
                    break;

                default:
                    break;
            }

            return keepPlaying;
        }
    }
}