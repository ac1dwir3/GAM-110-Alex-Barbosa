﻿namespace Midterm
{
    internal class Player
    {
        public int xPos;
        public int yPos;
        public bool[] inventory; //{Shield Slot, Weapon Slot, Key Slot}
        public int viewDistance;

        public Player(int x, int y)
        {
            xPos = x;
            yPos = y;
            inventory = new bool[] {false, false, false};
            viewDistance = 2;
        }

        //Adds Items To Player Inventory
        public void PickUpItem(char item)
        {
            switch (item)
            {
                case 'S':
                    inventory[0] = true;
                    break;
                case 'W':
                    inventory[1] = true;
                    break;
                case 'K':
                    inventory[2] = true;
                    break;
            }
        }

        //Movement
        internal void MoveUp()
        {
            yPos--;
        }
        internal void MoveDown()
        {
            yPos++;
        }
        internal void MoveLeft()
        {
            xPos--;
        }
        internal void MoveRight()
        {
            xPos++;
        }

    }
}