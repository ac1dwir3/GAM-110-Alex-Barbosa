﻿using System;

class Project_1
{
    public class Character
    {
        //Character Info
        public string CharacterName;

        public int CharacterClass = 0;
        public String[] classes = new String[] { "Runner", "Courier", "Thief", "Fugitive", "Custom", "Newbie"/*Randomized Stats*/ };

        //Character Stats
        public String[] statNames = new String[] { "Speed", "Endurance", "Dexterity", "Accuracy", "Strength", "Stealth", "Thievery" };
        public float[] stats = new float[] { 0, 0, 0, 0, 0, 0, 0 };
        public float skillPoints = 0;

        public static void NewCharacter(Character _character)
        {
            //Character Name
            Console.WriteLine("Name This Character:");
            _character.CharacterName = Console.ReadLine();
            Console.WriteLine();

            GetCharacterClass(_character);
            GetCharacterStats(_character);
            ReadCharacterSheet(_character);
        }

        public static void GetCharacterClass(Character namedCharacter)
        {
            //Character Class
            bool validchoice = false;
            while (!validchoice)
            {
                Console.WriteLine("CLASSES:");
                for (int i = 0; i < namedCharacter.classes.Length; i++)
                    Console.WriteLine((i + 1) + ". " + namedCharacter.classes[i]);

                Console.WriteLine();
                Console.Write("Choose A Character Class: ");
                string input = Console.ReadLine();
                bool isanumber = int.TryParse(input, out namedCharacter.CharacterClass);
                namedCharacter.CharacterClass--;
                validchoice = isanumber && (namedCharacter.CharacterClass) > -1 && (namedCharacter.CharacterClass) < namedCharacter.classes.Length;
                if (validchoice == false)
                    Console.WriteLine("Invalid Choice");

                Console.WriteLine();
            }
        }

        private static void GetCharacterStats(Character classedCharacter)
        {
            //Character Stats

            //Stats Based On Class
            //Default of 14 SkillPoints Destibuted Throughout Stats

            switch (classedCharacter.CharacterClass)
            {
                case 0://Runner
                    classedCharacter.skillPoints = 4; //4 Extra Skill Points
                    for (int i = 0; i < classedCharacter.stats.Length; i++)
                    {
                        classedCharacter.stats[i] = 2; //100% balanced In Everything
                    }
                    break;

                case 1://Courier

                    //Speed
                    classedCharacter.stats[0] = 4;
                    //Endurance
                    classedCharacter.stats[1] = 4;
                    //Dexterity
                    classedCharacter.stats[2] = 3;
                    //Accuracy
                    classedCharacter.stats[3] = 1;
                    //Strength
                    classedCharacter.stats[4] = 2;
                    //Stealth
                    classedCharacter.stats[5] = 0;
                    //Thievery
                    classedCharacter.stats[6] = 0;
                    break;

                case 2://Thief
                    //Speed
                    classedCharacter.stats[0] = 2;
                    //Endurance
                    classedCharacter.stats[1] = 2;
                    //Dexterity
                    classedCharacter.stats[2] = 3;
                    //Accuracy
                    classedCharacter.stats[3] = 1;
                    //Strength
                    classedCharacter.stats[4] = 0;
                    //Stealth
                    classedCharacter.stats[5] = 3;
                    //Thievery
                    classedCharacter.stats[6] = 3;
                    break;

                case 3://Fugitive
                    //Speed
                    classedCharacter.stats[0] = 4;
                    //Endurance
                    classedCharacter.stats[1] = 3;
                    //Dexterity
                    classedCharacter.stats[2] = 0;
                    //Accuracy
                    classedCharacter.stats[3] = 0;
                    //Strength
                    classedCharacter.stats[4] = 1;
                    //Stealth
                    classedCharacter.stats[5] = 3;
                    //Thievery
                    classedCharacter.stats[6] = 3;
                    break;

                case 4://Custom
                    classedCharacter.skillPoints = 14;
                    break;

                case 5://Newbie (Randomized Stats)
                    classedCharacter.skillPoints = 14;
                    Random random = new Random();

                    //Makes Sure Only 14 Skill Points Are Spent
                    while (classedCharacter.skillPoints != 0)
                    {
                        for (int i = 0; i < classedCharacter.stats.Length; i++)
                        {
                            //Assigns Random Value Between 0 and The Amount Of Skill Points Left To Each Stat
                            classedCharacter.stats[i] = random.Next(0, (int)classedCharacter.skillPoints + 1);

                            //Keeps Track Of Skill Points Spent
                            float spentPoints = classedCharacter.stats[i];
                            classedCharacter.skillPoints -= spentPoints;
                        }
                    }
                    break;
            }

            //Continues Until All SkillPoints Are Spent
            while (classedCharacter.skillPoints != 0)
            {
                //Asks Which Stat To Change
                int statToChange = 0;
                bool validchoice = false;
                while (!validchoice)
                {
                    for (int i = 0; i < classedCharacter.statNames.Length; i++)
                        Console.WriteLine((i + 1) + ". " + classedCharacter.statNames[i] + ": " + classedCharacter.stats[i]);
                    Console.WriteLine();

                    Console.WriteLine("Which Stat Do You Want To Edit?");
                    string input = Console.ReadLine();
                    bool isanumber = int.TryParse(input, out statToChange);
                    statToChange--;
                    validchoice = isanumber && (statToChange) > -1 && (statToChange) < classedCharacter.statNames.Length;
                    if (validchoice == false)
                    {
                        Console.WriteLine("Invalid Choice");
                        Console.WriteLine();
                    }
                }
                Console.WriteLine();

                //Asks How Many Skill Points To Spend
                float add = 0;
                validchoice = false;
                while (!validchoice)
                {
                    Console.WriteLine("Total Skill Points Remaining: " + classedCharacter.skillPoints);
                    Console.WriteLine(classedCharacter.statNames[statToChange] + ": " + classedCharacter.stats[statToChange]);
                    Console.WriteLine("How Many Skill Points Do You Want To Add To This Stat?");
                    string input = Console.ReadLine();
                    bool isanumber = float.TryParse(input, out add);
                    validchoice = isanumber && add > -1 && add <= classedCharacter.skillPoints;
                    if (validchoice == false)
                    {
                        Console.WriteLine("Invalid Choice");
                        Console.WriteLine();
                    }
                }
                Console.WriteLine();

                float newStat = classedCharacter.stats[statToChange] + add;
                classedCharacter.stats[statToChange] = newStat;

                classedCharacter.skillPoints -= add;
            }
        }
        public static void ReadCharacterSheet(Character createdCharacter)
        {
            //Character Sheet Readout
            Console.WriteLine("Name: " + createdCharacter.CharacterName);
            Console.WriteLine("Class: " + createdCharacter.classes[createdCharacter.CharacterClass]);
            Console.WriteLine("STATS: ");
            for (int i = 0; i < createdCharacter.statNames.Length; i++)
                Console.WriteLine((i + 1) + ". " + createdCharacter.statNames[i] + ": " + createdCharacter.stats[i]);

            Console.ReadLine();
        }

    }

    static void Main(string[] args)
    {
        Character Player = new Character();
        Character.NewCharacter(Player);

    }
}