﻿using System;

class Homework_2
{
    static void Main(string[] args)
    {
        Console.WriteLine("Welcome! Type the number that corresponds to your desired conversion.");
        Console.WriteLine();

        //Runs program until user inputs 0 in the menu
        ConversionMenu();
    }

    static void ConversionMenu()
    {

        string[] units = new string[] { "Quit Program", "Inches", "Feet", "Miles", "Centimeters", "Meters" };
        float[] baseConversions = new float[] { 0f, 1f, 12f, 5280f, 0.393701f, 39.3701f };
        int from = 0;
        int to = 0;
        float amount = 0;

        //Convert from
        bool validchoice = false;
        while (!validchoice)
        {
            Console.WriteLine("Which unit would you like to convert from?");
            for (int i = 1; i < units.Length; i++)
            {
                Console.WriteLine(i + ". " + units[i]);
            }
            Console.WriteLine("0. Quit Program");
            Console.WriteLine();

            string input = Console.ReadLine();
            bool isanumber = int.TryParse(input, out from);

            validchoice = isanumber && (from > -1) && (from < units.Length);
            if (validchoice == false)
            {
                Console.WriteLine("Invalid Choice");
                Console.WriteLine();
            }
        }
        Console.WriteLine();

        //Ends program if 0 is chosen
        if (from == 0)
        {
            Environment.Exit(0);
        }

        //Convert to
        validchoice = false;
        while (!validchoice)
        {
            Console.WriteLine("Which unit would you like to convert to?");
            for (int i = 1; i < units.Length; i++)
            {
                Console.WriteLine(i + ". " + units[i]);
            }
            Console.WriteLine("0. Quit Program");
            Console.WriteLine();

            string input = Console.ReadLine();
            bool isanumber = int.TryParse(input, out to);

            validchoice = isanumber && (to > -1) && (to < units.Length);
            if (validchoice == false)
                Console.WriteLine("Invalid Choice");
            Console.WriteLine();
        }
        Console.WriteLine();

        //Ends program if 0 is chosen
        if (to == 0)
        {
            Environment.Exit(0);
        }

        //Amount to convert
        validchoice = false;
        while (!validchoice)
        {
            Console.WriteLine("How many units would you like to convert?");
            Console.WriteLine("Type 0 to Quit Program");

            string input = Console.ReadLine();
            bool isanumber = float.TryParse(input, out amount);

            validchoice = isanumber;
            if (validchoice == false)
                Console.WriteLine("That's Not A Number");
            Console.WriteLine();
        }
        Console.WriteLine();

        //Ends program if 0 is chosen
        if (amount == 0)
        {
            Environment.Exit(0);
        }

        //Converts units
        float conversion = 0f;
        conversion = amount * (baseConversions[from] / baseConversions[to]);

        Console.WriteLine(amount + " " + units[from] + " = " + conversion + " " + units[to]);
    }
}
