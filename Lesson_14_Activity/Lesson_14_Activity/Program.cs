﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_14_Activity
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            IntHolder intholder = new IntHolder();
            intholder.num = 10;

            Console.WriteLine("x:" + x + " intholder:" + intholder.num);

            Set5(x);
            Set5(intholder);

            Console.WriteLine("x:" + x + " intholder:" + intholder.num);

            Console.ReadLine();
        }

        private static void Set5(IntHolder holder)
        {
            holder.num = 5;
        }

        private static void Set5(int num)
        {
            num = 5;
        }
    }

    class IntHolder
    {
        public int num;
    }
}
