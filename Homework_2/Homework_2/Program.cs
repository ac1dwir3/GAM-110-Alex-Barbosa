﻿using System;

class Homework_2
{
    static void Main(string[] args)
    {
        Console.WriteLine("Welcome! Type the number that corresponds to your desired conversion.");

        //Runs program until user inputs 0 in the menu
        ConversionMenu();
    }

    static void ConversionMenu()
    {
        Console.WriteLine("1. Convert inches to yards");
        Console.WriteLine("2. Convert inches to feet");
        Console.WriteLine("3. Convert feet to yards");
        Console.WriteLine("0. Quit Program");
        Console.WriteLine();
        int option = Int32.Parse(Console.ReadLine());
        Console.WriteLine();

        if (option == 1)
        {
            Console.WriteLine("How Many Inches");
            string input = Console.ReadLine();

            float measurement;
            float.TryParse(input, out measurement);

            float conversion = ConvertInchToYard(measurement);

            //Different possible outputs
            if (conversion == 1)
                Console.WriteLine(measurement + " inches = " + conversion + " yard");
            else if (measurement == 1)
                Console.WriteLine(measurement + " inch = " + conversion + " yards");
            else
                Console.WriteLine(measurement + " inches = " + conversion + " yards");

            //Restarts
            Console.WriteLine();
            ConversionMenu();
        }
        else if (option == 2)
        {
            Console.WriteLine("How Many Inches");
            string input = Console.ReadLine();

            float measurement;
            float.TryParse(input, out measurement);

            float conversion = ConvertInchToFeet(measurement);

            //Different possible outputs
            if(conversion == 1)
                Console.WriteLine(measurement + " inches = " + conversion + " foot");
            else if(measurement == 1)
                Console.WriteLine(measurement + " inch = " + conversion + " feet");
            else
                Console.WriteLine(measurement + " inches = " + conversion + " feet");

            //Restarts
            Console.WriteLine();
            ConversionMenu();
        }
        else if (option == 3)
        {
            Console.WriteLine("How Many Feet");
            string input = Console.ReadLine();

            float measurement;
            float.TryParse(input, out measurement);

            float conversion = ConvertFeetToYard(measurement);

            //Different possible outputs
            if (conversion == 1)
                Console.WriteLine(measurement + " feet = " + conversion + " yard");
            else if (measurement == 1)
                Console.WriteLine(measurement + " foot = " + conversion + " yards");
            else
                Console.WriteLine(measurement + " feet = " + conversion + " yards");

            //Restarts
            Console.WriteLine();
            ConversionMenu();
        }
        else if(option == 0)
        {
            //Ends Program
        }
        else
        {
            Console.WriteLine("That's not an option");
            Console.WriteLine();
            ConversionMenu();
        }
    }

    //Conversion Methods
    public static float ConvertInchToFeet(float _measurement)
    {
        float conversion = _measurement / 12;
        return conversion;
    }
    public static float ConvertInchToYard(float _measurement)
    {
        float conversion = ConvertFeetToYard(ConvertInchToFeet(_measurement));
        return conversion;
    }
    public static float ConvertFeetToYard(float _measurement)
    {
        float conversion = _measurement / 3;
        return conversion;
    }
}
