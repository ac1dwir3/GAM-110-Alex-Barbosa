﻿using System;

class Homework_1
{
    static void Main(string[] args)
    {

        int x = 20;
        int y = 10;

        Console.WriteLine(x + " + " + y + " = " + (x + y));

        Console.WriteLine(x + " - " + y + " = " + (x - y));

        Console.WriteLine(x + " * " + y + " = " + (x * y));

        Console.WriteLine(x + " / " + y + " = " + (x / y));

        Console.ReadLine();

    }
}
