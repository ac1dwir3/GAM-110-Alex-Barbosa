﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Homework_6
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> words = new Dictionary<string, int>();

            //Reads File "Input.txt" And Counts How Many Times A Word Shows Up
            StreamReader reader = new StreamReader("Input.txt");
            while (reader.Peek() != -1)
            {
                string line = reader.ReadLine();
                string[] newWords = line.Split(" ,./;‘’'[]<>?:—{}!@#$%^&*()“”_+-=`~".ToCharArray());

                foreach (string newWord in newWords)
                {
                    string key = newWord.ToLower();
                    if (words.ContainsKey(key))
                    {
                        words[key] += 1;
                    }
                    else
                    {
                        words.Add(key, 1);
                    }
                }
            }
            reader.Close();

            //Sorts Words In Dictionary Alphabetically
            var listWords = words.Keys.ToList();
            listWords.Sort();

            
            //Writes Dictionary Out To New File Called "Output.txt"
            StreamWriter writer = new StreamWriter("Output.txt");
            foreach(var key in listWords)
            {
                writer.WriteLine(key + ": " + words[key]);
            }
            writer.Close();
        }
    }
}
