﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Project
{
    //Ammunition Is An Item
    class Ammo : Item
    {
        //All Ammo Deals Ranged Damage
        public int rangedDamage;
    }
    //Different Types Of Ammo
    class PistolAmmo : Ammo
    {
        public PistolAmmo()
        {
            name = "Pistol Ammo";
            value = 5;
            rangedDamage = 10;
        }
        public PistolAmmo(int _amount)
        {
            name = "Pistol Ammo";
            value = 5;
            rangedDamage = 10;
            amount = _amount;
        }
    }
    class RifleAmmo : Ammo
    {
        public RifleAmmo()
        {
            name = "Rifle Ammo";
            value = 10;
            rangedDamage = 20;
        }
        public RifleAmmo(int _amount)
        {
            name = "Rifle Ammo";
            value = 10;
            rangedDamage = 20;
            amount = _amount;
        }
    }
    class ShotgunAmmo : Ammo
    {
        public ShotgunAmmo()
        {
            name = "Shotgun Ammo";
            value = 12;
            rangedDamage = 30;
        }
        public ShotgunAmmo(int _amount)
        {
            name = "Shotgun Ammo";
            value = 12;
            rangedDamage = 30;
            amount = _amount;
        }
    }
}
