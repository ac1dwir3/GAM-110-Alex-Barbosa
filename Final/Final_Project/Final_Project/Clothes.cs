﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Project
{
    //Clothes Are Items
    class Clothes : Item
    {
        //All Clothes Have Weight and Provides Some Type Of Protection Even If It Is 0
        public float weight;
        public float armor;
        //Weight Could Build Up And Slow The Player Down
    }
    //Different Clothes
    class ChestWear : Clothes
    {
        public ChestWear(string _name, float _weight, float _armor)
        {
            name = _name;
            //All Chest Wear Starts At 20 Dollars And Increases In Price Based On How Heavy It Is Allowing For Different Chest Wear To Have Different Value
            value = 20 + (5 * _weight);
            weight = _weight;
            armor = _armor;
        }
        public ChestWear(string _name, float _weight, float _armor, int _amount)
        {
            name = _name;
            value = 20 + (5 * _weight);
            weight = _weight;
            armor = _armor;
            amount = _amount;
        }
    }
    class LegWear : Clothes
    {
        public LegWear(string _name, float _weight, float _armor)
        {
            name = _name;
            //All Leg Wear Starts At 20 Dollars And Increases In Price Based On How Heavy It Is Allowing For Different Leg Wear To Have Different Value
            value = 10 + (5 * _weight);
            weight = _weight;
            armor = _armor;
        }
        public LegWear(string _name, float _weight, float _armor, int _amount)
        {
            name = _name;
            value = 10 + (5 * _weight);
            weight = _weight;
            armor = _armor;
            amount = _amount;
        }
    }
    class FootWear : Clothes
    {
        //Different Foot Wear Are Faster Than Others
        public float speed;
        public FootWear(string _name, float _weight, float _armor, float _speed)
        {
            name = _name;
            //All Foot Wear Starts At 20 Dollars And Increases In Price Based On How Heavy It Is Allowing For Different Foot Wear To Have Different Value
            value = 50 + (5 * _weight);
            weight = _weight;
            armor = _armor;
            speed = _speed;
        }
        public FootWear(string _name, float _weight, float _armor, float _speed, int _amount)
        {
            name = _name;
            value = 50 + (5 * _weight);
            weight = _weight;
            armor = _armor;
            speed = _speed;
            amount = _amount;
        }
    }
}
