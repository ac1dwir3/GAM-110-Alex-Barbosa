﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Project
{
    class Player
    {
        //Every Player Has A Name, Starts With $200, And Standard Running Shoes In Their Inventory
        public string name;
        public float playerMoney = 200;
        public List<Item> playerInventory;

        public Player(string _name)
        {
            name = _name;
            playerInventory = new List<Item> { /*new FootWear("Running Shoes", .2f, .1f, 1)*/ };
        }
    }
}
