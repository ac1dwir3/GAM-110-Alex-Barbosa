﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Project
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player("Alex");
            EnterShop(player);
        }
        public static void EnterShop(Player _player)
        {
            Console.WriteLine("Would You Like To Enter The Shop? (Y/N)");
            ConsoleKey input = Console.ReadKey().Key;
            Console.WriteLine();
            switch (input)
            {
                case ConsoleKey.Y:
                    Console.Clear();
                    Shop shop = new Shop();

                    bool shopping = true;
                    while (shopping)
                    {
                        shop.DisplayShop(_player);
                        shopping = shop.GetInput(_player);
                    }
                    break;
                case ConsoleKey.N:
                    break;
                default:
                    Console.WriteLine("Invalid Input");
                    EnterShop(_player);
                    break;
            }
        }
    }
}
