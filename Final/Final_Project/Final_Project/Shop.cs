﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Project
{
    class Shop
    {
        //Every Shop Has A Stock To Sell And Money To Buy Your Items
        public float shopMoney;
        public List<Item> stock;

        public Shop()
        {
            //Shopkeppers Can Have Between $1000 And $2000 To Buy Yoour Items
            Random moneyGenerator = new Random();
            shopMoney = moneyGenerator.Next(1000, 2000);
            stock = new List<Item> { new Rifle("M16", 0.3f, 35), new PistolAmmo(), new Pistol("Revolver", 0.5f, 6), new Knife("Dagger", 5), new ChestWear("Balistic Vest", 10, 30) };
        }

        public void DisplayShop(Player player)
        {
            Console.Clear();

            int indexer = 1;
            //Displays Player's Inventory
            foreach(Item item in player.playerInventory)
            {
                Console.WriteLine($"{indexer}. {item.name}(x{item.amount}): ${item.value} Each");
                indexer++;
            }
            
            //Displays Shop's Inventory
            indexer = 1;
            foreach(Item item in stock)
            {
                Console.SetCursorPosition(50, indexer - 1);
                Console.WriteLine($"{indexer}. {item.name}: ${item.value} Each");

                indexer++;
                Console.ForegroundColor = ConsoleColor.White;
            }

            //Divides Menus
            for (int i = 0; i < indexer+3; i++)
            {
                Console.SetCursorPosition(45, i);
                Console.WriteLine("|");
            }

            //Displays Player's And Shopkeeper's Money
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.SetCursorPosition(0, indexer + 1);
            Console.WriteLine("Your Money: $" + player.playerMoney);
            Console.SetCursorPosition(50, indexer + 1);
            Console.WriteLine("Shopkeeper's Money: $" + shopMoney);

            Console.ForegroundColor = ConsoleColor.White;
        }

        public bool GetInput(Player player)
        {
            bool shopping = true;

            Console.WriteLine();
            Console.WriteLine("Would You Like To Buy Something Or Sell Something? (Press 'B' To Buy Or 'S' To Sell)");
            Console.WriteLine("Press 'Q' Or 'Escape' To Leave The Store");
            ConsoleKey input = Console.ReadKey().Key;
            switch (input)
            {
                //Quits Game
                case ConsoleKey.Q:
                    shopping = false;
                    break;
                case ConsoleKey.Escape:
                    shopping = false;
                    break;

                //Buy Or Sell
                case ConsoleKey.B:
                    //Only Allows To Buy Items If Player Has More Money Than The Cheapest Item In The Shop
                    if (player.playerMoney > CheapestItem(stock))
                    {
                        Buying(player);
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("You Don't Have Enough Money To Buy Anything In This Shop!");
                        GetInput(player);
                    }
                    break;
                case ConsoleKey.S:
                    //Only Allows To Sell Items If Player Inventory Has An Item
                    if(player.playerInventory.Count > 0)
                    {
                        Selling(player);
                    }
                    else
                    {
                        Console.WriteLine();
                        Console.WriteLine("You Don't Have Anything To Sell!");
                        GetInput(player);
                    }
                    break;
            }

            return shopping;
        }

        //Buying Mode
        public void Buying(Player player)
        {
            DisplayShop(player);
            bool buying = true;
            while(buying)
            {
                //Selects The Number Of The Item In The Shop Menu
                int itemChosen = 0;
                bool validchoice = false;
                while (!validchoice)
                {
                    Console.WriteLine();
                    Console.WriteLine("Type The Number Of The Item You Want To Buy Or Type '0' To Cancel");
                    string input = Console.ReadLine();
                    bool isanumber = int.TryParse(input, out itemChosen);
                    validchoice = isanumber && (itemChosen) > 0 && (itemChosen) <= stock.Count && player.playerMoney >= stock[itemChosen - 1].value;
                    if (validchoice == false)
                    {
                        if(itemChosen <= 0)
                        {
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Invalid Choice");

                            if(!(itemChosen > stock.Count))
                                if (player.playerMoney < stock[itemChosen - 1].value)
                                    Console.WriteLine("You Can Afford That Item!");
                        }
                    }
                }

                //Checks If Player Pressed Anything Less Than Or Equal To '0'
                if (itemChosen <= 0)
                {
                    break;
                }

                itemChosen--;
                //Selects The Amount Of That Item To Buy
                int amountWanted = 0;
                validchoice = false;
                while (!validchoice)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Type How Many Of The {stock[itemChosen].name} You Want To Buy Or Type '0' To Cancel");
                    string input = Console.ReadLine();
                    bool isanumber = int.TryParse(input, out amountWanted);
                    validchoice = isanumber && (amountWanted) >= 0 && player.playerMoney >= (amountWanted * stock[itemChosen].value);
                    if (validchoice == false)
                    {
                        Console.WriteLine("Invalid Choice");

                        if(player.playerMoney < (amountWanted * stock[itemChosen].value))
                            Console.WriteLine("You Cant Afford That Many!");
                    }
                }

                //Checks If Player Pressed Anything Less Than Or Equal To '0'
                if (amountWanted <= 0)
                {
                    break;
                }

                //Confirms Purchase
                Console.WriteLine();
                Console.WriteLine("Comfirm Action (Y/N)");
                ConsoleKey confirmation = Console.ReadKey().Key;
                switch (confirmation)
                {
                    case ConsoleKey.Y:
                        //Adds Purchaced Item To Player's Inventory
                        Item purchased = stock[itemChosen];
                        bool added = false;
                        foreach (Item item in player.playerInventory)
                        {
                            //If Item Already Owned, Increase Amount Owned
                            if (item.name == purchased.name)
                            {
                                item.amount += amountWanted;
                                added = true;
                            }
                        }
                        //If Item Not Already Owned, Add It To Inventory
                        if (added == false && amountWanted > 0)
                        {
                            purchased.amount = amountWanted;
                            player.playerInventory.Add(purchased);
                        }

                        //Decreased Player's Money
                        player.playerMoney -= purchased.value * amountWanted;
                        //Increases Shopkeeper's Money
                        shopMoney += purchased.value * amountWanted;

                        //If Player Still Has Enough Money To Buy An Item In The Shop Continue In The Buying Mode
                        if(player.playerMoney > CheapestItem(stock))
                        {
                            DisplayShop(player);
                        }
                        //If Player's Money Is Lower Than The Cheapest Item In The Shop Leave Buying Mode
                        else
                        {
                            buying = false;
                            DisplayShop(player);
                            GetInput(player);
                        }
                        break;

                    case ConsoleKey.N:
                        buying = false;
                        DisplayShop(player);
                        GetInput(player);
                        break;
                }
            }
        }
        //Selling Mode
        public void Selling(Player player)
        {
            DisplayShop(player);
            bool selling = true;
            while (selling)
            {
                //Selects The Number Of The Item In Player's Inventory
                int itemChosen = 0;
                bool validchoice = false;
                while (!validchoice)
                {
                    Console.WriteLine();
                    Console.WriteLine("Type The Number Of The Item You Want To Sell Or Type '0' To Cancel");
                    string input = Console.ReadLine();
                    bool isanumber = int.TryParse(input, out itemChosen);
                    validchoice = isanumber && (itemChosen) > 0 && (itemChosen) <= player.playerInventory.Count && shopMoney >= player.playerInventory[itemChosen - 1].value;
                    if (validchoice == false)
                    {
                        if(itemChosen <= 0)
                        {
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Invalid Choice");

                            if(!(itemChosen < player.playerInventory.Count))
                                if (shopMoney < player.playerInventory[itemChosen - 1].value)
                                    Console.WriteLine("The Shopkeeper Can't Afford That Item!");
                        }
                    }
                }

                //Checks If Player Pressed Anything Less Than Or Equal To '0'
                if (itemChosen <= 0)
                {
                    break;
                }

                itemChosen--;

                //Selects The Amount Of That Item To Sell
                int amountSelling = 0;
                validchoice = false;
                while (!validchoice)
                {
                    Console.WriteLine();
                    Console.WriteLine($"Type How Many Of The {player.playerInventory[itemChosen].name} You Want To Sell Or Type '0' To Cancel");
                    string input = Console.ReadLine();
                    bool isanumber = int.TryParse(input, out amountSelling);
                    validchoice = isanumber && (amountSelling) >= 0 && amountSelling <= player.playerInventory[itemChosen - 1].value && shopMoney >= (amountSelling * player.playerInventory[itemChosen - 1].value);
                    if (validchoice == false)
                    {
                        Console.WriteLine("Invalid Choice");

                        if (shopMoney < (amountSelling * player.playerInventory[itemChosen].value))
                            Console.WriteLine("The Shopkeeper Cant Afford That Many!");
                    }
                }

                //Checks If Player Pressed Anything Less Than Or Equal To '0'
                if (amountSelling <= 0)
                {
                    break;
                }

                //Confirms Sale
                Console.WriteLine();
                Console.WriteLine("Comfirm Sale (Y/N)");
                ConsoleKey confirmation = Console.ReadKey().Key;
                switch (confirmation)
                {
                    case ConsoleKey.Y:
                        //Increases Player's Money
                        player.playerMoney += player.playerInventory[itemChosen].value * amountSelling;
                        //Decrease Shopkeeper's Money
                        shopMoney -= player.playerInventory[itemChosen].value * amountSelling;

                        //If Selling Less Than Max Amount Of The Selected Item, Decrease Amount Of The Item You Own
                        //(Selling 2 Out Of 3 Of An Item Will Leave 1 Of That Item Remaining)
                        if(player.playerInventory[itemChosen].amount > amountSelling)
                        {
                            player.playerInventory[itemChosen].amount -= amountSelling;
                        }
                        //If Selling Max Amout Of The Selected Item, Remove Item From Inventory
                        //(Selling 3 Out Of 3 Of An Item Will Leave 0 Of That Item, Removing It From Your Inventory)
                        else
                        {
                            player.playerInventory.Remove(player.playerInventory[itemChosen]);
                        }

                        //If Player Still Has Potential Items To Sell And The Shopkeep Has Enough Money To Buy Your Cheapest Item Continue In Selling Mode
                        if (player.playerInventory.Count > 0 && CheapestItem(player.playerInventory) < shopMoney)
                        {
                            DisplayShop(player);
                        }
                        //If Player Has No Items To Sell Or Shopkeeper Can't Afford Your Items Leave Selling Mode
                        else
                        {
                            selling = false;
                            DisplayShop(player);
                            GetInput(player);
                        }
                        break;

                    case ConsoleKey.N:
                        selling = false;
                        DisplayShop(player);
                        GetInput(player);
                        break;
                }
            }
        }

        //Finds Cheapest Item In An Inventory
        public float CheapestItem(List<Item> items)
        {
            float cheapestItem = items[0].value;
            for (int i = 0; i < items.Count() - 1; i++)
            {
                if (items[i].value < items[i + 1].value)
                    cheapestItem = items[i].value;
                else
                    cheapestItem = items[i + 1].value;
            }

            return cheapestItem;
        }

    }
}
