﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Final_Project
{
    //Weapons Are Items
    class Weapon : Item
    {
        //All Weapons Deal Melee Damage And Have A Melee Attack Radius (How Far They Can Reach)
        public int meleeDamage;
        public float meleeRadius;
    }
    //Different Types Of Weapons

    //Melee Weapons
    class MeleeWeapon : Weapon
    {
        //Melee Attack Radius Can Vary Based On The Wingspan Of The Player Which Is Determined By The Character's Height When Creating Their Character And The Length Of The Weapon
        //A Taller Character Can Reach Further Or Jump Higher Than A Shorter Person But May Not Be As Fast or Fit Into Smaller Areas (Vents, ect.)
    }
    //Knives Are Melee Weapons
    class Knife : MeleeWeapon
    {
        public Knife(string _name, int damage)
        {
            name = _name;
            meleeDamage = damage;
            value = 10;
        }
        public Knife(string _name, int damage, int _amount)
        {
            name = _name;
            meleeDamage = damage;
            value = 10;
            amount = _amount;
        }
    }


    //Ranged Weapons
    class RangedWeapon : Weapon
    {
        //All Ranged Weapons Have A Maximun Amount Of Ammo It Can Shoot Before Reloading, Different Rates Of Fire, And A Maximum Range It Can Shoot
        public int magSize;
        public float rateOfFire;
        public float maxRange;
    }
    //Pistols Are Ranged Weapons
    class Pistol : RangedWeapon
    {
        public Pistol(string _name, float ROF, int mag)
        {
            //Guns Have Different Names (Luger, Revolver, etc.)
            name = _name;
            //Worth $100
            value = 100;
            //Max Bullets Gun Can Shoot Before Needing To Be Reloaded
            magSize = mag;
            //Deals 4 Damage When Used In Melee Attack
            meleeDamage = 4;
            //Time Between Shots In Seconds (Rate Of Fire)
            rateOfFire = ROF;
            //Can Shoot Up To 100 Meters
            maxRange = 200;
        }
        public Pistol(string _name, float ROF, int mag, int _amount)
        {
            name = _name;
            value = 100;
            magSize = mag;
            meleeDamage = 4;
            rateOfFire = ROF;
            maxRange = 200;
            //How Many Are Available
            amount = _amount;
        }
    }
    //Rifles Are Ranged Weapons
    class Rifle : RangedWeapon
    {
        public Rifle(string _name, float ROF, int mag)
        {
            name = _name;
            value = 500;
            magSize = mag;
            meleeDamage = 7;
            rateOfFire = ROF;
            maxRange = 500;
        }
        public Rifle(string _name, float ROF, int mag, int _amount)
        {
            name = _name;
            value = 500;
            magSize = mag;
            meleeDamage = 7;
            rateOfFire = ROF;
            maxRange = 500;
            amount = _amount;
        }
    }
    //Shotguns Are Ranged Weapons
    class Shotgun : RangedWeapon
    {
        public Shotgun(string _name, float ROF, int mag)
        {
            name = _name;
            value = 300;
            magSize = mag;
            meleeDamage = 8;
            rateOfFire = ROF;
            maxRange = 50;
        }
        public Shotgun(string _name, float ROF, int mag, int _amount)
        {
            name = _name;
            value = 300;
            magSize = mag;
            meleeDamage = 8;
            rateOfFire = ROF;
            maxRange = 50;
            amount = _amount;
        }
    }
}
