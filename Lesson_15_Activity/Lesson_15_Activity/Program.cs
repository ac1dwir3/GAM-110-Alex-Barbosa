﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_15_Activity
{
    class Program
    {
        static void Main(string[] args)
        {
            int x = 10;
            Console.WriteLine(x);
            Set5(out x);
            Console.WriteLine(x);

            int y = 10;
            Console.WriteLine(y);
            Set6(ref y);
            Console.WriteLine(y);
            Console.ReadLine();
        }

        static void Set5(out int num)
        {
            num = 5;
        }
        static void Set6(ref int num)
        {
            num = 6;
        }
    }
}
