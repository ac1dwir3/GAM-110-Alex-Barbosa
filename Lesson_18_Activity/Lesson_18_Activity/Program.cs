﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson_18_Activity
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] myList = { 3, 4, 4, 5, 7, 3, 2, 5, 8, 9, 11, 0, 1, 2, 6, 4, 1, 12 };
            DisplayList(myList);
            bool keepGoing = true;
            while(keepGoing)
            {
                keepGoing = false;
                for (int i = 0; i < myList.Length - 1; i++)
                {
                    int item1 = myList[i];
                    int item2 = myList[i + 1];
                    if(item1 > item2)
                    {
                        myList[i] = item2;
                        myList[i + 1] = item1;
                        keepGoing = true;
                    }
                }
            }
            DisplayList(myList);

            Console.ReadLine();

        }

        private static void DisplayList(int[] list)
        {
            foreach (int item in list)
            {
                Console.Write(item + " ");
            }
            Console.WriteLine();

        }
    }
}
