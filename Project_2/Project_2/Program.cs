﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Board tictactoe = new Board(3, 3);
            PlayGame(tictactoe);
        }
        static void PlayGame(Board _board)
        {
            bool keepPlaying = true;
            while (keepPlaying)
            {
                _board.DrawBoard();
                _board.TakeTurn();
                if (_board.CheckWin() != ' ')
                {
                    _board.DrawBoard();
                    Console.WriteLine("Player: " + _board.CheckWin() + " Wins");
                    keepPlaying = false;
                }
                else if (_board.CheckDraw())
                {
                    _board.DrawBoard();
                    Console.WriteLine("It's A Draw");
                    keepPlaying = false;
                }
            }

            if (_board.AskContinue() == 'Y')
            {
                _board.ResetBoard();
                PlayGame(_board);
            }
        }
    }
}
