﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_2
{
    public class Board
    {
        //Board Info
        int rows;
        int cols;
        char[,] board;
        char blank = ' ';
        int inARow;

        //Player Info
        char player1;
        char player2;
        char currentplayer;

        public Board(int rowsandcols, int InARowToWin)
        {
            inARow = InARowToWin;
            rows = rowsandcols;
            cols = rowsandcols;
            board = new char[rows, cols];
            FillCells();

            player1 = 'X';
            player2 = 'O';
            currentplayer = player1;
        }

        private void FillCells()
        {
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < rows; col++)
                {
                    board[row, col] = blank;
                }
            }
        }

        public void DrawBoard()
        {
            Console.Clear();
            //Game Board
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < rows; col++)
                {
                    string DisplayChar = $"[{board[col, row]}]";
                    Console.Write(DisplayChar);
                }
                Console.WriteLine();
            }
            Console.WriteLine();
            //Reference Board
            for (int row = 0, i = 1; row < rows; row++)
            {
                for (int col = 0; col < rows; col++, i++)
                {
                    string DisplayChar = $"[{i}]";
                    Console.Write(DisplayChar);
                }
                Console.WriteLine();
            }
        }

        public void TakeTurn()
        {
            bool validmove = false;
            while (!validmove)
            {
                Console.WriteLine("Choose A Cell Player: " + currentplayer);
                string input = Console.ReadLine();
                bool isanumber = int.TryParse(input, out int choice);
                if (isanumber && choice < board.Length + 1 && choice > -1)
                {
                    int col = (choice - 1) / cols;
                    int row = (choice - 1) % rows;
                    if (board[row, col] == blank)
                    {
                        board[row, col] = currentplayer;
                        validmove = true;
                    }
                    else
                        Console.WriteLine("Invalid Input");
                }
                else
                    Console.WriteLine("Invalid Input");
            }

            SwapPlayer();
        }

        private void SwapPlayer()
        {
            if (currentplayer == player1)
                currentplayer = player2;
            else
                currentplayer = player1;
        }

        public char CheckWin()
        {
            int p1win;
            int p2win;

            //Checks Rows
            for (int row = 0; row < rows; row++)
            {
                p1win = 0;
                p2win = 0;
                for (int col = 0; col < cols; col++)
                {
                    if (board[row, col] == player1)
                    {
                        p1win++;
                        p2win = 0;
                    }
                    else if (board[row, col] == player2)
                    {
                        p2win++;
                        p1win = 0;
                    }
                    else
                    {
                        p1win = 0;
                        p2win = 0;
                    }
                    if (p1win == inARow)
                        return player1;
                    if (p2win == inARow)
                        return player2;
                }
            }

            //Checks Columns
            for (int col = 0; col < cols; col++)
            {
                p1win = 0;
                p2win = 0;
                for (int row = 0; row < rows; row++)
                {
                    if (board[row, col] == player1)
                    {
                        p1win++;
                        p2win = 0;
                    }
                    else if (board[row, col] == player2)
                    {
                        p2win++;
                        p1win = 0;
                    }
                    else
                    {
                        p1win = 0;
                        p2win = 0;
                    }
                    if (p1win == inARow)
                        return player1;
                    if (p2win == inARow)
                        return player2;
                }

            }

            //Checks Diagonals Downwards
            for (int rowstart = 0; rowstart < rows; rowstart++)
                for (int colstart = 0; colstart < cols; colstart++)
                {
                    p1win = 0;
                    p2win = 0;
                    for (int row = rowstart, col = 0; row < rows && col < cols; row++, col++)
                    {
                        if (board[row, col] == player1)
                        {
                            p1win++;
                            p2win = 0;
                        }
                        else if (board[row, col] == player2)
                        {
                            p2win++;
                            p1win = 0;
                        }
                        else
                        {
                            p1win = 0;
                            p2win = 0;
                        }

                        if (p1win == inARow)
                            return player1;
                        if (p2win == inARow)
                            return player2;
                    }
                }

            //Checks Diagonals Upwards
            for (int rowstart = 0; rowstart < rows; rowstart++)
                for (int colstart = 0; colstart < cols; colstart++)
                {
                    p1win = 0;
                    p2win = 0;
                    for (int row = rowstart, col = 0; row > 0 && col < cols; row--, col++)
                    {
                        if (board[row, col] == player1)
                        {
                            p1win++;
                            p2win = 0;
                        }
                        else if (board[row, col] == player2)
                        {
                            p2win++;
                            p1win = 0;
                        }
                        else
                        {
                            p1win = 0;
                            p2win = 0;
                        }

                        if (p1win == inARow)
                            return player1;
                        if (p2win == inARow)
                            return player2;
                    }
                }

            return blank;
        }

        public bool CheckDraw()
        {
            bool draw = false;
            int filledCells = 0;

            if (CheckWin() == blank)
            {
                for (int row = 0; row < rows; row++)
                {
                    for (int col = 0; col < cols; col++)
                    {
                        if (board[row, col] == player1 || board[row, col] == player2)
                            filledCells++;
                    }
                }

                if (filledCells == rows * cols)
                    draw = true;
            }
            return draw;
        }

        public char AskContinue()
        {
            bool _continue = false;
            char choice = 'N';

            bool validchoice = false;
            while (!validchoice)
            {
                Console.WriteLine("Do You Want To Play Again: Y / N");
                string input = Console.ReadLine();
                bool isanumber = int.TryParse(input, out int tempNumber);
                bool isachar = char.TryParse(input, out choice);
                if (isachar == true && (choice == 'y' || choice == 'Y' || choice == 'N' || choice == 'n') && isanumber == false)
                {
                    if (choice == 'y')
                    {
                        choice = 'Y';
                        _continue = true;
                    }
                    validchoice = true;
                }
                else
                    Console.WriteLine("Invalid Input");
            }

            if (_continue)
            {
                ResetBoard();
                return choice;
            }
            else
                return choice;
        }

        public void ResetBoard()
        {
            Console.Clear();
            FillCells();
            currentplayer = player1;
        }

    }
}
