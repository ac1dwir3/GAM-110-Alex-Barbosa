﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3
{
    class Object
    {
        public int x;
        public int y;
        public char identifier;

        public Object(int _x, int _y, char _identifier)
        {
            x = _x;
            y = _y;
            identifier = _identifier;
        }

        public void DisplayObject()
        {
            Console.SetCursorPosition(x, y);
            Console.Write(identifier);
        }

        public void RemoveObject()
        {
            Console.SetCursorPosition(x, y);
            Console.Write(" ");
        }

        //Getters
        public int GetX()
        {
            return x;
        }
        public int GetY()
        {
            return y;
        }

    }
}
