﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3
{
    class Maze
    {

        int rows = 20;
        int cols = 20;
        char[,] cells;
        public Player player;
        public Object key;
        public Object door;
        public Object end;

        public bool escaped = false;

        int movescounter = 0;

        public Maze()
        {
            key = new Object (16, 9, 'K');
            door = new Object (11, 15, 'D');
            end = new Object(18, 18, 'E');

            CreateMaze();

            player = new Player(1,1);
        }

        private void CreateMaze()
        {
            cells = new char[,] {{'#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' },
                                { '#',' ',' ','#','#','#','#','#','#','#','#','#','#','#','#','#',' ','#','#','#' },
                                { '#',' ',' ','#','#',' ',' ',' ',' ',' ',' ','#','#','#','#','#',' ','#','#','#' },
                                { '#','#',' ','#',' ',' ','#','#',' ','#',' ','#','#',' ',' ',' ',' ',' ',' ','#' },
                                { '#','#',' ','#',' ','#',' ',' ',' ','#',' ',' ',' ',' ','#',' ','#','#',' ','#' },
                                { '#','#',' ',' ',' ','#',' ','#','#','#',' ','#','#','#',' ',' ',' ','#',' ','#' },
                                { '#','#','#',' ','#','#',' ','#','#','#',' ',' ',' ','#',' ','#',' ',' ',' ','#' },
                                { '#',' ',' ',' ','#','#',' ','#','#','#',' ','#',' ','#',' ','#',' ',' ',' ','#' },
                                { '#',' ','#',' ',' ',' ',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ','#' },
                                { '#',' ','#','#',' ','#','#','#','#','#',' ','#',' ','#',' ','#','#','#',' ','#' },
                                { '#',' ','#','#',' ',' ','#','#','#','#',' ','#',' ','#',' ','#','#','#',' ','#' },
                                { '#',' ','#','#','#',' ',' ',' ',' ','#',' ','#','#',' ',' ',' ','#','#',' ','#' },
                                { '#',' ',' ',' ','#','#',' ','#',' ','#',' ',' ','#',' ','#',' ',' ',' ','#','#' },
                                { '#','#','#',' ','#','#',' ','#',' ',' ','#',' ','#',' ','#','#','#',' ','#','#' },
                                { '#','#',' ',' ','#',' ',' ',' ','#',' ',' ',' ',' ',' ',' ',' ','#',' ','#','#' },
                                { '#','#',' ','#','#',' ','#',' ',' ','#','#','#',' ','#','#',' ','#',' ',' ','#' },
                                { '#',' ',' ',' ',' ',' ',' ',' ','#',' ','#','#',' ',' ',' ',' ','#',' ',' ','#' },
                                { '#',' ','#',' ','#','#','#',' ','#',' ','#','#','#',' ','#',' ','#',' ','#','#' },
                                { '#',' ','#',' ',' ',' ',' ',' ','#',' ',' ',' ',' ',' ','#',' ','#',' ',' ','#' },
                                { '#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#','#' }};
        }

        public void DisplayMaze()
        {

            Console.Clear();
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++)
                {
                    switch (cells[row, col])
                    {
                        case '#':
                            Console.BackgroundColor = ConsoleColor.White;
                            break;

                        default:
                            break;

                    }

                    DrawCell(row, col);
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }
            }

            CheckStatus(player);

            //Displays Key In Green If Player Doesn't Yet Have It
            if (!player.HasItem(key))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                key.DisplayObject();
            }

            //Displays Door In Red If Player Doesn't Yet Have The Key
            if(!player.HasItem(key))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                door.DisplayObject();
            }

            //Displays The End Of Maze With Blue Background
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            end.DisplayObject();

            
            //Displays Player In White
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            player.DisplayPlayer();


        }

        private void DrawCell(int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(cells[x, y]);
        }

        private void CheckStatus(Player _player)
        {
            //Checks If Player Has Reached The Key
            if (player.GetX() == key.GetX() && player.GetY() == key.GetY())
            {
                if (!player.HasItem(key))
                    player.GrabItem(key);
            }

            //Checks If Player Has Key In Inventory
            if (player.HasItem(key))
            {
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ForegroundColor = ConsoleColor.White;
                Console.SetCursorPosition(0, 21);
                Console.WriteLine("You Have A Key! A Door Has Unlocked!");
                Console.SetCursorPosition(player.GetX(), player.GetY());
            }

            //Checks If Player Has Ecaped Maze
            if (player.GetX() == end.GetX() && player.GetY() == end.GetY())
            {
                WinGame();
            }
        }

        private void WinGame()
        {
            Console.Clear();
            Console.WriteLine($"You've Escaped The Maze in {movescounter} Moves!");
            escaped = true;
        }

        public void GetInput()
        {
            ConsoleKey input = Console.ReadKey().Key;

            switch (input)
            {
                case ConsoleKey.LeftArrow:
                    if(cells[player.GetX() - 1, player.GetY()] != '#')
                    {
                        if (player.GetX() == door.GetX() && player.GetY() == door.GetY())
                        {
                            if (player.HasItem(key))
                            {
                                player.MoveLeft();
                                movescounter++;
                            }
                            else
                                break;
                        }
                        else
                        {
                            player.MoveLeft();
                            movescounter++;
                        }
                    }
                    break;

                case ConsoleKey.UpArrow:
                    if (cells[player.GetX(), player.GetY() - 1] != '#')
                    {
                        if (player.GetX() == door.GetX() && player.GetY() == door.GetY())
                        {
                            if (player.HasItem(key))
                            {
                                player.MoveUp();
                                movescounter++;
                            }
                            else
                                break;
                        }
                        else
                        {
                            player.MoveUp();
                            movescounter++;
                        }
                    }

                    break;

                case ConsoleKey.RightArrow:
                    if (cells[player.GetX() + 1, player.GetY()] != '#')
                    {
                        if (player.GetX() == door.GetX() && player.GetY() == door.GetY())
                        {
                            if (player.HasItem(key))
                            {
                                player.MoveRight();
                                movescounter++;
                            }
                            else
                                break;
                        }
                        else
                        {
                            player.MoveRight();
                            movescounter++;
                        }
                    }
                    break;

                case ConsoleKey.DownArrow:
                    if (cells[player.GetX(), player.GetY() + 1] != '#')
                    {
                        if (player.GetX() == door.GetX() && player.GetY() == door.GetY())
                        {
                            if (player.HasItem(key))
                            {
                                player.MoveDown();
                                movescounter++;
                            }
                            else
                                break;
                        }
                        else
                        {
                            player.MoveDown();
                            movescounter++;
                        }
                    }
                    break;

                default:
                    break;
            }
        }

    }
}
