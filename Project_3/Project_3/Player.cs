﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3
{
    class Player
    {
        public int x;
        public int y;

        public Object[] inventory;

        public Player(int _x, int _y)
        {
            x = _x;
            y = _y;

            inventory = new Object[] { };
        }

        public void DisplayPlayer()
        {
            Console.SetCursorPosition(x, y);
            Console.Write("P");
        }

        //Movement
        public void MoveUp()
        {
            y--;
        }
        public void MoveDown()
        {
            y++;
        }
        public void MoveLeft()
        {
            x--;
        }
        public void MoveRight()
        {
            x++;
        }

        public void GrabItem(Object item)

        {
            inventory = new Object[] { item };
        }

        public bool HasItem(Object item)
        {
            for (int i = 0; i < inventory.Length; i++)
                if (inventory[i] == item)
                    return true;

            return false;
        }

        //Getters
        public int GetX()
        {
            return x;
        }
        public int GetY()
        {
            return y;
        }
    }
}
