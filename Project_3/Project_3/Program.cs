﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Maze maze = new Maze();
            while (!maze.escaped)
            {
                maze.DisplayMaze();
                maze.GetInput();
            }
        }
    }
}
